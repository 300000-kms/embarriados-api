# Embarriados API

API for https://embarriados.cotec.es/

## Method `nodes`:

Parameters:
- `method`: method [mandatory]
- `epoch`: epoch `1` or `2` [mandatory]

Example: https://uia.300000.eu/assu/?method=nodes&epoch=1

## Method `nodespro`:

Parameters:
- `method`: method [mandatory]

Example: https://uia.300000.eu/assu/?method=nodespro

## Method `areas`:

Parameters:
- `method`: method [mandatory]
- `co`: community code [mandatory]

Example: https://uia.300000.eu/assu/main.cgi?method=areas&co=182M

## Method `edges`:

Parameters:
- `method`: method [mandatory]
- `epoch`: epoch `1` or `2` [mandatory]
- `co`: community code [mandatory]
- `di`: direction `from` or `to` [mandatory]

Example: https://uia.300000.eu/assu/main.cgi?method=edges&epoch=1&co=182M&di=from

## Method `rentas`:

Parameters:
- `method`: method [mandatory]

Example: https://uia.300000.eu/assu/?method=rentas

## Method `rentas_stats`:

Parameters:
- `method`: method [mandatory]

Example: https://uia.300000.eu/assu/?method=rentas_stats

## Method `nodes_usos`:

Parameters:
- `method`: method [mandatory]

Example: https://uia.300000.eu/assu/?method=nodes_usos

## Method `nodes_usos_resumen`:

Parameters:
- `method`: method [mandatory]

Example: https://uia.300000.eu/assu/?method=nodes_usos_resumen

## Method `nodes_rentas`:

Parameters:
- `method`: method [mandatory]

Example: https://uia.300000.eu/assu/?method=nodes_rentas