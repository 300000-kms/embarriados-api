<?php 
    class Database {
        private $db_rentas = "data/rentas.sqlite";
        private $db_areas = "data/areas.sqlite";
        private $db_edges_1 = "data/matriz_20191100_lines.sqlite";
        private $db_nodes_1 = "data/matriz_20191100_points.sqlite";
        private $db_edges_2 = "data/matriz_20200416_lines.sqlite";
        private $db_nodes_2 = "data/matriz_20200416_points.sqlite";
        private $db_usos = "data/db_usos.sqlite";
        private $db_model = "data/model_full_5.sqlite";

        public $conn_rentas;
        public $conn_areas;
        public $conn_edges_1;
        public $conn_nodes_1;
        public $conn_edges_2;
        public $conn_nodes_2;
        public $conn_usos;
        public $conn_model;

        public function getConnectionRentas(){
            $this->conn_rentas = null;
            try {
                $this->conn_rentas = new PDO("sqlite:".$this->db_rentas); 
                $this->conn_rentas->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_rentas;
        }

        public function getConnectionAreas(){
            $this->conn_areas = null;
            try {
                $this->conn_areas = new PDO("sqlite:".$this->db_areas); 
                $this->conn_areas->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_areas;
        }

        public function getConnectionEdges1(){
            $this->conn_edges_1 = null;
            try {
                $this->conn_edges_1 = new PDO("sqlite:".$this->db_edges_1);
                $this->conn_edges_1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_edges_1;
        }

        public function getConnectionNodes1(){
            $this->conn_nodes_1 = null;
            try {
                $this->conn_nodes_1 = new PDO("sqlite:".$this->db_nodes_1);
                $this->conn_nodes_1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_nodes_1;
        }

        public function getConnectionEdges2(){
            $this->conn_edges_2 = null;
            try {
                $this->conn_edges_2 = new PDO("sqlite:".$this->db_edges_2);
                $this->conn_edges_2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_edges_2;
        }

        public function getConnectionNodes2(){
            $this->conn_nodes_2 = null;
            try {
                $this->conn_nodes_2 = new PDO("sqlite:".$this->db_nodes_2);
                $this->conn_nodes_2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_nodes_2;
        }

        public function getConnectionUsos(){
            $this->conn_usos = null;
            try {
                $this->conn_usos = new PDO("sqlite:".$this->db_usos);
                $this->conn_usos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_usos;
        }


        public function getConnectionModel(){
            $this->conn_model = null;
            try {
                $this->conn_model = new PDO("sqlite:".$this->db_model);
                $this->conn_model->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $exception){
                echo "Database could not be connected: " . $exception->getMessage();
            }
            return $this->conn_model;
        }
    }  
?>