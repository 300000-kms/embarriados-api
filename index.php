<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once 'database.php';
    include_once 'queries.php';

    if (isset($_GET['method']) && !empty($_GET['method'])) {

        $method = htmlspecialchars($_GET['method']);

        if ($method == 'edges' ||
            $method == 'nodes' ||
            $method == 'nodespro' ||
            $method == 'areas' ||
            $method == 'rentas' ||
            $method == 'rentas_stats' ||
            $method == 'nodes_usos' ||
            $method == 'nodes_usos_resumen' ||
            $method == 'nodes_rentas') {

            $database = new Database();
            $msg = array();

            try {

                if ($method == 'edges') {

                    // https://uia.300000.eu/assu/api/main.cgi?method=edges&epoch=1&co=182M&di=from
                    // https://uia.300000.eu/assu/api/main.cgi?method=edges&epoch=2&co=182M&di=from

                    if (isset($_GET['epoch']) && !empty($_GET['epoch']) && is_numeric($_GET['epoch']) && in_array($_GET['epoch'], ['1', '2']) && isset($_GET['co']) && !empty($_GET['co']) && isset($_GET['di']) && !empty($_GET['di']) && in_array($_GET['di'], ['from', 'to'])) {

                        $epoch = (int)$_GET['epoch'];
                        $co = htmlspecialchars($_GET['co']);
                        $di = htmlspecialchars($_GET['di']);

                        if ($epoch == 1)
                            $db = $database->getConnectionEdges1();
                        else if ($epoch == 2)
                            $db = $database->getConnectionEdges2();
                        $query = new Query($db);
                        $rows = $query->selectEdges($co, $di);

                        echo json_encode($rows);
                    }
                    else {
                        echo json_encode(array("error" => "No valid GET parameter or empty: try with numeric 'epoch' 1 or 2, co and di 'from' or 'to'"));
                    }

                }

                else if ($method == 'nodes') {

                    // https://uia.300000.eu/assu/?method=nodes&epoch=1
                    // https://uia.300000.eu/assu/?method=nodes&epoch=2

                    if (isset($_GET['epoch']) && !empty($_GET['epoch']) && is_numeric($_GET['epoch']) && in_array($_GET['epoch'], ['1', '2'])) {

                        $epoch = (int)$_GET['epoch'];

                        if ($epoch == 1)
                            $db = $database->getConnectionNodes1();
                        else if ($epoch == 2)
                            $db = $database->getConnectionNodes2();
                        $query = new Query($db);
                        $rows = $query->selectNodes($epoch);

                        foreach ($rows as $row) {
                            $msg[$row['co']] = $row;
                        }

                        echo json_encode($msg);
                    }
                    else {
                        echo json_encode(array("error" => "No valid GET parameter or empty: try with numeric 'epoch' 1 or 2"));
                    }
                }

                else if ($method == 'nodespro') {

                    // https://uia.300000.eu/assu/?method=nodespro

                    $db1 = $database->getConnectionNodes1();
                    $query1 = new Query($db1);
                    $rows1 = $query1->selectNodesPro1();

                    foreach ($rows1 as $row) {
                        $msg[$row['co']] = $row;
                    }

                    $db2 = $database->getConnectionNodes2();
                    $query2 = new Query($db2);
                    $rows2 = $query2->selectNodesPro2();

                    foreach ($rows2 as $row) {
                        if (array_key_exists($row['co'], $msg)) {
                            $msg[$row['co']]['pr_2'] = $row['pr_2'];
                            $msg[$row['co']]['ps_2'] = $row['ps_2'];
                            $msg[$row['co']]['pc_2'] = $row['pc_2'];
                            $msg[$row['co']]['pe_2'] = $row['pe_2'];
                        }
                        else {
                            $msg[$row['co']] = $row;
                        }
                    }

                    echo json_encode($msg);
                }

                else if ($method == 'areas') {

                    // https://uia.300000.eu/assu/api/main.cgi?method=areas&co=182M

                    if (isset($_GET['co']) && !empty($_GET['co'])) {

                        $co = htmlspecialchars($_GET['co']);
                        $db = $database->getConnectionAreas();
                        $query = new Query($db);
                        $result = $query->selectAreas($co);

                        echo json_encode(json_decode($result['geometry']));
                    }
                    else {
                        echo json_encode(array("error" => "No valid GET parameter or empty: try with 'co'"));
                    }
                }

                else if ($method == 'rentas') {

                    // https://uia.300000.eu/assu/?method=rentas

                    $db = $database->getConnectionRentas();
                    $query = new Query($db);
                    $rows = $query->selectRentas();

                    foreach ($rows as $row) {
                        $msg[$row['co']] = $row;
                    }

                    echo json_encode($msg);
                }

                else if ($method == 'rentas_stats') {

                    // https://uia.300000.eu/assu/?method=rentas_stats

                    $db = $database->getConnectionRentas();
                    $query = new Query($db);
                    $result = $query->selectRentasStats();

                    echo json_encode($result);
                }

                else if ($method == 'nodes_usos') {

                    // https://uia.300000.eu/assu/?method=nodes_usos

                    $db = $database->getConnectionUsos();
                    $query = new Query($db);
                    $rows = $query->selectNodesUsos();

                    foreach ($rows as $row) {
                        $msg[$row['co']] = $row;
                    }

                    echo json_encode($msg);
                }

                else if ($method == 'nodes_usos_resumen') {

                    // https://uia.300000.eu/assu/?method=nodes_usos_resumen

                    $db = $database->getConnectionUsos();
                    $query = new Query($db);
                    $result = $query->selectNodesUsosResumen();

                    echo json_encode($result);
                }

                else if ($method == 'nodes_rentas') {

                    // https://uia.300000.eu/assu/?method=nodes_rentas

                    $db = $database->getConnectionModel();
                    $query = new Query($db);
                    $rows = $query->selectNodesRentas();

                    foreach ($rows as $row) {
                        $msg[$row['co']] = $row;
                    }

                    echo json_encode($msg);
                }
            }

            catch(PDOException $e) {
                echo json_encode(array("error" => $e->getMessage()));
            }
        }
        else {
            echo json_encode(array("error" => "No valid method: must be 'edges', 'nodes', 'nodespro', 'areas', 'rentas', 'rentas_stats', 'nodes_usos', 'nodes_usos_resumen' or 'nodes_rentas'"));
        }
    }
    else {
        echo json_encode(array("error" => "No valid GET parameter or empty: try with 'method'"));
    }

?>