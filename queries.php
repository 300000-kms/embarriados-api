<?php
    class Query {
        private $conn;

        public function __construct($db){
            $this->conn = $db;
        }

        public function selectEdges($co="", $di="") {

            if ($di == 'from') {
                $sqlQuery =
                    'SELECT celda_destino, flujo ' .
                    'FROM data ' .
                    'WHERE celda_origen LIKE "' . $co . '"';
            }
            else if ($di == 'to') {
                $sqlQuery =
                    'SELECT celda_origen as celda_destino, flujo ' .
                    'FROM data ' .
                    'WHERE celda_destino LIKE "' . $co . '"';
            }

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function selectNodes() {
            $sqlQuery =
                'SELECT celda_origen as co, ' .
                    'cast(o_x as real) o_x, ' .
                    'cast(o_y as real) o_y, ' .
                    's_pob_resid as pr, ' .
                    's_pob_sale as ps, ' .
                    's_pob_casa as pc, ' .
                    'flujo_entra as pe ' .
                'FROM data';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt;
        }

        public function selectNodesPro1() {
            $sqlQuery =
                'SELECT celda_origen as co, ' .
                    'cast(o_x as real) o_x, ' .
                    'cast(o_y as real) o_y, ' .
                    's_pob_resid as pr_1, ' .
                    's_pob_sale as ps_1, ' .
                    's_pob_casa as pc_1, ' .
                    'flujo_entra as pe_1 ' .
                'FROM data';

            //error_log($sqlQuery);

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt;
        }

        public function selectNodesPro2() {
            $sqlQuery =
                'SELECT celda_origen as co, ' .
                    'cast(o_x as real) o_x, ' .
                    'cast(o_y as real) o_y, ' .
                    's_pob_resid as pr_2, ' .
                    's_pob_sale as ps_2, ' .
                    's_pob_casa as pc_2, ' .
                    'flujo_entra as pe_2 ' .
                'FROM data';

            //error_log($sqlQuery);

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt;
        }

        public function selectAreas($co = "") {
            $sqlQuery =
                'SELECT geometry ' .
                'FROM data ' .
                'WHERE id_grupo LIKE "' . $co . '"';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function selectRentas() {
            $sqlQuery =
                'SELECT id_grupo as co, ' .
                    'pob_grupo, nombre_cel, sum_pob_as, pob_area_geo_sum, renta_mediana_unidad_de_consumo, renta_mediana_unidad_de_consumo_20200416, renta_mediana_unidad_de_consumo_20191100, variacion, dn_20200416, dn_20191100, dn_var, x, y ' .
                'FROM data';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt;
        }

        public function selectRentasStats() {
            $sqlQuery =
                'SELECT min("dn_var") dn_var_min, ' .
                    'max("dn_var") dn_var_max, ' .
                    'min("dn_20200416") dn_20200416_min, ' .
                    'max("dn_20200416") dn_20200416_max, ' .
                    'min("dn_20191100") dn_20191100_min, ' .
                    'max("dn_20191100") dn_20191100_max  ' .
                'FROM data';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function selectNodesUsos() {
            $sqlQuery =
                'SELECT muni as co, ' .
                    'muni as nombre_cel, ' .
                    'vivienda/total as v, ' .
                    'comercial/total as c, ' .
                    'productivo/total as p, ' .
                    'dotacion/total as d, ' .
                    'agricola/total as g, ' .
                    'almacenaje/total as a, ' .
                    'aparcamiento/total as o, ' .
                    'resto/total as r, ' .
                    'total as total ' .
                'FROM data';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt;
        }

        public function selectNodesUsosResumen() {
            $sqlQuery =
                'SELECT sum(vivienda) as v, ' .
                    'sum(productivo) as p, ' .
                    'sum(comercial) as c, ' .
                    'sum(dotacion) as d, ' .
                    'sum(agricola) as g, ' .
                    'sum(almacenaje) as a, ' .
                    'sum(aparcamiento) as o, ' .
                    'sum(resto) as r ' .
                'FROM data';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt->fetch();
        }

        public function selectNodesRentas() {
            $sqlQuery =
                'SELECT nombre_cel as co, ' .
                    'cluster, ' .
                    'indicador, ' .
                    'mod__rnt_rnt_co_med_pon as renta ' .
                'FROM model_full_5';

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            return $stmt;
        }
    }
?>